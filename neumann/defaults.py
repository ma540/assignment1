import numpy as np

def u_0(x, func='sin'):
    y = np.zeros_like(x)
    if func == 'square':
        y[np.where(abs(x)<0.5)] = 1
    if func == 'triangle':
        y = 1 - abs(x)
    if func == 'zero':
        y = np.zeros_like(x)
    if func == 'sin':
        y = np.sin(np.pi*x)
    return y

def u_l(t):
    #return np.minimum(t,np.ones_like(t))
    return np.sin(np.pi*(-1-t))
def u_r(t):
    #return 0*np.minimum(t,np.ones_like(t))
    return np.sin(np.pi*(1-t))
def u_t_0(x):
    #return np.ones_like(x)
    return -np.pi*np.cos(np.pi*x)
def true(x,t):
    return np.sin(np.pi*(x-t))
