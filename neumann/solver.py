import numpy as np
import matplotlib.pyplot as plt
from defaults import *


def solver(dt = 2e-1, Th = 1.0, u_0 = u_0, u_l = u_l, u_r = u_r, method=1, dx=2e-1, u_t_0=u_t_0, true=true):
    l = (dt/dx)**2
    num_steps = int(Th/dt)
    num_points = int(2/dx) + 1

    x = np.linspace(-1,1, num_points)
    t = np.linspace(0,Th, num_steps)

    X,T = np.meshgrid(x,t)
    u = np.zeros_like(X)

    u[0,:] = u_0(x)
    u[:,0] = u_l(t)
    u[:,-1] = u_r(t)

    if method==1:
        u_1 = u_t_0(x)*dt + u[0,:]
    elif method == 2:
        u_1 = (2*u[0,:]*(1-l) + l*(np.roll(u[0,:],1) + np.roll(u[0,:],-1)) + 2*u_t_0(x)*dt)/2

    u[1,1:-1] = u_1[1:-1]

    A = np.diag((-2*l +2)*np.ones_like(x)) + l*(np.diag(np.ones_like(x[:-1]), -1) + np.diag(np.ones_like(x[:-1]), 1))

    for k in range(1,num_steps-1):
        u_new = A.dot(u[k,:]) - u[k-1,:]
        u[k+1,1:-1] = u_new[1:-1]
    
    error = np.mean(abs(u[-1,:] - true(x,T[-1,:])))
    return error, X,T,u


