from solver import solver
import matplotlib.pyplot as plt
import numpy as np

#Initial condition
def u_0(x, func='sin'):
    y = np.zeros_like(x)
    if func == 'square':
        y[np.where(abs(x)<0.5)] = 1
    if func == 'triangle':
        y = 1 - abs(x)
    if func == 'zero':
        y = np.zeros_like(x)
    if func == 'sin':
        y = np.sin(np.pi*x)
    if func == 'sign':
        y = np.sign(x)
    return y

#left BC
def u_l(t, func='sin'):
    if func == 'ramp':
        return np.minimum(t,np.ones_like(t))
    if func == 'sin':
        return np.sin(np.pi*(-1-t))
    if func == 'zero':
        return np.zeros_like(t)
    if func == 'one':
        return np.ones_like(t)

#Right BC
def u_r(t, func='sin'):
    if func == 'ramp':
        return np.minimum(t,np.ones_like(t))
    if func == 'sin':
        return np.sin(np.pi*(1-t))
    if func == 'zeros':
        return np.zeros_like(t)
    if func == 'one':
        return np.ones_like(t)

#Initial Neumann BC
def u_t_0(x, func='sin'):
    if func == 'zero':
        return np.zeros_like(x)
    if func == 'sin':
        return -np.pi*np.cos(np.pi*x)

#True solution
def true(x,t):
    return np.sin(np.pi*(x-t))

def sinwave():
    fig,axs = plt.subplots(1, 2, figsize=(8,4))
    ax = axs[0]
    error, X,T,u=solver(Th=4.0, dt=1e-1, dx=2e-1, method=1)
    cs = ax.contourf(X,T,u,200, vmin=-1.0, vmax=1.0)
    ax.set_title("First order")
    ax.set_xlabel('$x$')
    ax.set_ylabel('$t$')
    
    ax = axs[1]
    error, X,T,u=solver(Th=4.0, dt=1e-1, dx=2e-1, method=2)
    cs = ax.contourf(X,T,u,200, vmin=-1.0, vmax=1.0)
    ax.set_xlabel('$x$')
    ax.set_yticks([])
    ax.set_title("Second order")

    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, 0.125, 0.025, 0.75])
    cbar = fig.colorbar(cs, cax=cbar_ax)
    cbar.ax.set_ylabel('$u$')
    #fig.colorbar()
    plt.savefig('sin.png')
    plt.show()

def triangular():
    tri = lambda x: u_0(x, func='triangle')
    ut0 = lambda x: u_t_0(x, func='zero')
    ul = lambda t: u_l(t, func="zero")
    ur = lambda t: u_r(t, func="zero")

    dx = 1e-2
    dt = 1e-3
    Th = 1.0

    fig,axs = plt.subplots(1, 2, figsize=(8,4))
    ax = axs[0]
    error, X, T, u = solver(Th=Th, dt=dt, dx=dx, method=1, u_0=tri, u_t_0=ut0, u_l=ul, u_r=ur)
    cs = ax.contourf(X,T,u,200, vmin=0.0, vmax=1.0)
    ax.set_title("First order")
    ax.set_xlabel('$x$')
    ax.set_ylabel('$t$')
    
    ax = axs[1]
    error, X,T,u=solver(Th=Th, dt=dt, dx=dx, method=2, u_0=tri, u_t_0=ut0, u_l=ul, u_r=ur)
    cs = ax.contourf(X,T,u,200, vmin=0.0, vmax=1.0)
    ax.set_xlabel('$x$')
    ax.set_yticks([])
    ax.set_title("Second order")

    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, 0.125, 0.025, 0.75])
    cbar = fig.colorbar(cs, cax=cbar_ax)
    cbar.ax.set_ylabel('$u$')
    #fig.colorbar()
    plt.savefig('tri.png')
    plt.show()

def shock():
    tri = lambda x: u_0(x, func='sign')
    ut0 = lambda x: u_t_0(x, func='zero')
    ul = lambda t: -1*u_l(t, func="one")
    ur = lambda t: u_r(t, func="one")

    dx = 1e-2
    dt = 1e-3
    Th = 2.0

    fig,axs = plt.subplots(1, 2, figsize=(8,4))
    ax = axs[0]
    error, X, T, u = solver(Th=Th, dt=dt, dx=dx, method=1, u_0=tri, u_t_0=ut0, u_l=ul, u_r=ur)
    cs = ax.contourf(X,T,u,200, vmin=-1.0, vmax=1.0)
    ax.set_title("First order")
    ax.set_xlabel('$x$')
    ax.set_ylabel('$t$')
    
    ax = axs[1]
    error, X,T,u=solver(Th=Th, dt=dt, dx=dx, method=2, u_0=tri, u_t_0=ut0, u_l=ul, u_r=ur)
    cs = ax.contourf(X,T,u,200, vmin=-1.0, vmax=1.0)
    ax.set_xlabel('$x$')
    ax.set_yticks([])
    ax.set_title("Second order")

    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, 0.125, 0.025, 0.75])
    cbar = fig.colorbar(cs, cax=cbar_ax)
    cbar.ax.set_ylabel('$u$')
    #fig.colorbar()
    plt.savefig('shock.png')
    plt.show()
def ramp():
    tri = lambda x: u_0(x, func='zero')
    ut0 = lambda x: u_t_0(x, func='zero')
    ul = lambda t: u_l(t, func="ramp")
    ur = lambda t: u_r(t, func="ramp")

    dx = 1e-2
    dt = 1e-3
    Th = 1.6

    fig,axs = plt.subplots(1, 2, figsize=(8,4))
    ax = axs[0]
    error, X, T, u = solver(Th=Th, dt=dt, dx=dx, method=1, u_0=tri, u_t_0=ut0, u_l=ul, u_r=ur)
    cs = ax.contourf(X,T,u,200, vmin=0.0, vmax=1.0)
    ax.set_title("First order")
    ax.set_xlabel('$x$')
    ax.set_ylabel('$t$')
    
    ax = axs[1]
    error, X,T,u=solver(Th=Th, dt=dt, dx=dx, method=2, u_0=tri, u_t_0=ut0, u_l=ul, u_r=ur)
    cs = ax.contourf(X,T,u,200, vmin=0.0, vmax=1.0)
    ax.set_xlabel('$x$')
    ax.set_yticks([])
    ax.set_title("Second order")

    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, 0.125, 0.025, 0.75])
    cbar = fig.colorbar(cs, cax=cbar_ax)
    cbar.ax.set_ylabel('$u$')
    #fig.colorbar()
    plt.savefig('ramp.png')
    plt.show()
def conv_plot():
    errors_1 = []
    errors_2 = []
    DT = np.linspace(1e-4,2e-1,41)
    for dt in DT:
        error, _,_,_ = solver(Th=10.0,dt=dt, dx=2e-1, method=1)
        errors_1.append(error)
        error, _,_,_ = solver(Th=10.0,dt=dt, dx=2e-1, method=2)
        errors_2.append(error)

    fig,ax = plt.subplots(figsize=(6,6))
    ax.plot(DT,errors_1, label="First order")
    ax.plot(DT,errors_2, label="Second order")
    ax.set_xlabel(r'$\Delta t$')
    ax.set_ylabel(r'Mean absolute error')
    plt.legend()
    plt.savefig('conv.png')
    plt.show()

conv_plot()
sinwave()
shock()
#triangular()
ramp()
